# User Privilege Analysis

This project allows you to easily browse user privilege information within the Symitar Core system.

## Install It

Place the following files in a folder either on an internal website or on your local PC. It will work on either, **a server is not required**.
- GroupView.html
- PrivView.html
- UserView.html
- Jquery-1.10.2.min.js

Upload the following PowerOns to Symitar:
- DWB.JSONUSERS.PN
- DWB.JSONSECGROUPS.PN
- DWB.JSONPRIVS.PN


## Run It

Run the three PowerOns within Batch Control or create a job to run all three of them.
The PowerOns will create three files: 
- Users.js
- SecGroups.js
- Permissions.js

Transfer these three files to the folder where you placed the html files. 
Open any of the three html files in a web browser and that's it.
I usually start with the UserView.html file first, but that's up to you!

## PowerTip

Setup a scheduled job using automation to run the PowerOns and transfer the files from your core system to the location of the html files.
Do this and you'll always have a current analysis of your user privilege records in Symitar.
